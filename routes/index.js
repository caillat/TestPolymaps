var express = require('express');
var fs = require('fs'), PNG = require('node-png').PNG;
var router = express.Router();
const path = require("path");

var ENTER = function () { console.log(arguments.callee.name + ": entering."); };
var EXIT = function () { console.log(arguments.callee.name + ": exiting."); };

var PNG_ROOT_DIR = '/home/caillat/TestPolymaps/public/';

/* GET home page. */
router.get('/', function(req, res, next) {
  ENTER();

  var path_to_fits = req.query.path;
  console.log("path_to_fits=", path_to_fits);

  // retrieve the product name, i.e. the simple filename without extension nor prefix.
  var product = path.parse(path_to_fits).name;
  console.log("product=", product);

  // we need a FITS header
  client.invoke("setData",encodeURI(path_to_fits), "aaaaaaaa", function (error, res_header, more) {
    if (error) {
      var message = error.toString();
      console.log(message);
    }
    else {
      var header = JSON.parse(res_header);

      let params = {
        title: 'Express',
        path: path_to_fits,
        product: product,
        header: header
      };
      res.render('index', params);
    }
  });
  EXIT();
});


router.get('/sumpng', function (req, res, next) {
  var sliceIndex0 = req.query.si0;
  var sliceIndex1 = req.query.si1;
  var path_to_fits = req.query.path;

  client.invoke("getSummedSliceRangeAsPNG", req.sessionID, sliceIndex0, sliceIndex1, path_to_fits, function (error, png, more) {
    if (error){
      console.log(error);
    }
    else {
      res.set({'Content-type': 'image/png'});
      res.send(png);
    }
  });
});

router.get('/:product/:sliceIndex', function(req, res, next) {
  sliceIndex = req.params['sliceIndex'];
  product = req.params['product'];

  var files = fs.readdirSync(PNG_ROOT_DIR+product);
  console.log("Number of files " + files.length);
  let sliceNumber = files.length;

  fs.createReadStream(PNG_ROOT_DIR+product+'/'+sliceIndex+'.png')
  .pipe(new PNG({
    filterType: -1
  }))
  .on('metadata', function() {
    res.render('index', { title: 'Express', op: 'slice', sliceIndex:sliceIndex, product:product, width:this.width, height:this.height, sliceNumber : sliceNumber });
  });

});

/*
** A route to display a spectrum
*/
router.get('/spectrum/:path'), function(req, res, next) {
  path = req.params['path'];

  client.invoke("setData", path, req.sessionID, function (error, res, more) {
    if (error) {
      var message = error.toString();
      console.log(message);
    }
    else {
      header = JSON.parse(res);
      res.render('index', {title: 'Express', op: 'spectrum', header:header})
    }
  });
}

router.post('/', function(req, res, next) {
  console.log("router.post('/', function(req, res, next) { : entering");
  var method = req.body.method;
  var self = res;
  var header;

  console.log("method = " + method);

  if (method === "getHMSDMS") {
    var ra = req.body.ra;
    var dec = req.body.dec;


    client.invoke("degToHMSDMS", parseInt(ra), parseInt(dec), req.body.fileName, req.sessionID, function (error, res, more) {
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: res }));
    });
  }

  if (method === "getRangeHMS") {
    var startR = req.body.start;
    var endR = req.body.end;
    var step = req.body.step;

    client.invoke("rangeToHMS", startR, endR, step, req.body.fileName, req.sessionID, function (error, res, more) {
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: res }));
    });
  }

  else if (method === "getRangeDMS") {
    startR = req.body.start;
    endR = req.body.end;
    step = req.body.step;

    client.invoke("rangeToDMS", startR, endR, step, req.body.fileName, req.sessionID, function (error, res, more) {
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify(result));
    });
  }

  else if (method == "RADECRangeInDegrees") {
      client.invoke("RADECRangeInDegrees", req.body.fileName, req.sessionID, function (error, res, more) {
        self.setHeader('Content-type', 'application/json');
        let result = {data: res};
        console.log ("About to return '" + JSON.stringify(result, 0, 4) + "'");
        self.send(JSON.stringify({data: res}));
      });
  }

  else if (method === "getHeader") {
    var fileName = req.body.name;

    client.invoke("setData", fileName, req.sessionID, function (error, res, more) {
      if (error) {
        var message = error.toString();
        console.log(message);
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ error: message }));
      } else {
        header = JSON.parse(res);
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: {displayLimits: displayLimits, header:header }}));
      }
    });
  }

  else if(method === "getSlice"){
    var sliceB = req.body.slice ? req.body.slice : 0;
    step = parseInt(req.body.step) ? parseInt(req.body.step) : 1;

    client.invoke("getSlice", sliceB, step, req.body.fileName, req.sessionID, function(error, res, more) {
      console.log("getSlice: ");
      self.setHeader('Content-Type', 'application/json');
      console.log("getSlice : start sending slice to client");
      self.send(JSON.stringify({ data: res }));
      console.log("getSlice : finished sending slice to client");
    });
  }

  else if (method === "getAverage") {
    var zmin = parseInt(req.body.zmin) ? parseInt(req.body.zmin) : 0;
    var zmax = parseInt(req.body.zmax) ? parseInt(req.body.zmax) : null;
    step = parseInt(req.body.step) ? parseInt(req.body.step) : 1;

    client.invoke("getAverage", req.body.fileName, req.sessionID, step, zmin, zmax, function(error, res, more) {
      console.log("getAverage:");
      self.setHeader('Content-Type', 'application/json');
      console.log("getAverage : start sending slice to client");
      self.send(JSON.stringify({ data: res }));
      console.log("getAverage : finished sending slice to client");
    });
  }

  else if(method === "getFreq") {
    var x = req.body.x ? req.body.x : 0;
    var y = req.body.y ? req.body.y : 0;

    client.invoke("getFreq", req.body.fileName, req.sessionID, parseInt(x), parseInt(y), function(error, res, more) {
      console.log("getFreq: ");
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: res }));
    });
  }

  else if(method === "getFreqAverage"){
    var xmin = parseInt(req.body.xmin) ? parseInt(req.body.xmin) : 0;
    var ymin = parseInt(req.body.ymin) ? parseInt(req.body.ymin) : 0;
    var xmax = parseInt(req.body.xmax)|| null;
    var ymax = parseInt(req.body.ymax) || null;

    //       client.invoke("getFreqAverage", req.body.fileName, req.sessionID, xmin, xmax, ymin, ymax, function(error, res, more) {
    client.invoke("getFreqAverage", req.body.fileName, req.sessionID, ymin, ymax, xmin, xmax, function(error, res, more) {
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: res }));
    });
  }

  else if (method === "getObjects") {
    // Peform a distinct query against the a field Header.OBJECT
    Fitsinfo.distinct('Header.OBJECT', function(err, objects) {
      if (err) {
        console.log(err);
      }
      else {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: objects.sort(function (a, b) {
          var aA = a.toUpperCase();
          var bB = b.toUpperCase();
          return (aA < bB) ? -1 : (aA > bB) ? 1 : 0;
        })
      }));
    }
  });
  }

else if (method === "getFiles") {
  var object = req.body.object;

  // Peform a distinct query against the a field Path with field Header.OBJECT == object
  Fitsinfo.distinct('Path', {"Header.OBJECT": object}, function (err, files) {
    if (err) {
      console.log(err);
    } else {
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({data: files}));
    }
  });
}

else if (method === "getFitsHeader") {
  var path = req.body.path;

  // Find the corresponding entry in DB
  //{fields: {"_id": 0}
  Fitsinfo.find({"Path": path}, {"_id": 0}, function(err, fitsHeader) {
    if (err) {
      console.log(err);
    } else {
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({data: fitsHeader[0]}));
    }
  });
}


else if (method === "getHistory") {
  self.setHeader('Content-Type', 'application/json');
  self.send(JSON.stringify({data: req.user.recentFiles}));
}
else {
  console.log("Do not know what to do here with " + JSON.stringify(req.body, null, 4));
}
console.log("router.post('/', function(req, res, next) { : exiting");

});

/*
** In response to a POST, returns a slice of the current datacube as a 2D image in PNG format
** along with statistics on the physical values of the slice.
** the slice index, i.e. the index corresponding to one given frequency, is
** passed in the field 'si' of the query along with some statistics on the
**
*/
router.post('/png', function(req, res, next) {
  console.log("router.post('/png', function(req, res, next) { : entering");
  var sliceIndex = req.body.si;
  var path_to_fits = req.body.path;

  client.invoke("getOneSliceAsPNG", sliceIndex, path_to_fits, function (error, result, more) {
    console.log('client.invoke("getOneSliceAsPNG", sliceIndex, path_to_fits, function (error, result, more) { : entering');
    if (error) {
    }
    else {
      res.send(result);
    }
    console.log('client.invoke("getOneSliceAsPNG", sliceIndex, path_to_fits, function (error, result, more) { : exiting');
  });
  console.log("router.post('/png', function(req, res, next) { : exiting");
});

router.post('/sumpng', function(req, res, next) {
  console.log("router.post('/sumpng', function(req, res, next) {: entering");
  var sliceIndex0 = req.body.si0;
  var sliceIndex1 = req.body.si1;
  var path_to_fits = req.body.path;
  client.invoke('getSummedSliceRangeAsPNG', req.sessionID, sliceIndex0, sliceIndex1, path_to_fits, function (error, result, more) {
    console.log("client.invoke('getSummedSliceRangeAsPNG', req.sessionID, sliceIndex0, sliceIndex1, path_to_fits, function (error, result, more) {: entering");
    if (error){
      console.log(error);
    }
    else {
      result["path_to_png"] = result["path_to_png"].toString(); // ??? why do I get an object instead of a string . I have no idea; this does not happen with post('/png') !!!
      res.send(result);
    }
    console.log("client.invoke('getSummedSliceRangeAsPNG', req.sessionID, sliceIndex0, sliceIndex1, path_to_fits, function (error, result, more) {: exiting");
  });
  console.log("router.post('/sumpng', function(req, res, next) {: exiting");
})

module.exports = router;
