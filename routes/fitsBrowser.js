var express = require('express');
var router = express.Router();
const request = require('request');

var miningPyHost = "http://juliette.obspm.fr"
var miningPyPort = "4250"

var clienthttp = {
  server : miningPyHost+":"+miningPyPort+"/artemix",
  getEntries : function(relKey, callback) {
    let input = {
      "relKey" : relKey
    };
    request.post(this.server+"/getEntries", {json:true, body: input}, function(error, response, body) {
      callback(error, response, body)
    });
  }
};

/***********************/
/*     GET routes     */
/***********************/
router.get("/", function (req, res, next) {
    console.log('router.get("/", function (req, res, next) { : entering');
    res.render("fitsBrowser", {});
    console.log('router.get("/", function (req, res, next) { : exiting');
});

router.get("/getEntries", function(req, res, next){
  console.log('router.get("/", function(req, res, next){ : entering');
  console.log("req.query = " + JSON.stringify(req.query, 0, 4));
  var relKey = req.query["key"];
  clienthttp.getEntries(relKey, (error, response, body) => {
    console.log("clienthttp.getEntries(relKey, (error, response, body) => { : entering");
    console.log("error " + JSON.stringify(error, 0, 4));
    console.log("response " + JSON.stringify(response, 0, 4));

    if (error) {
      var message = error.toString();
      console.log(message);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({"status" : false, "message": message }));
    }
    else if (response["statusCode"] == 500) {
      res.send(JSON.stringify({"status": false, "message": response["body"]}))
    }
    else {
      res.send(body["result"]);
    }
    console.log("clienthttp.getEntries(relKey, (error, response, body) => { : exiting");
  });
  console.log('router.get("/", function(req, res, next){ : exiting');
});

module.exports = router;
