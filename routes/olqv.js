var express = require('express');
var fs = require('fs'), PNG = require('node-png').PNG;
var router = express.Router();
const path = require("path");
const uuidv1 = require('uuid/v1');
const request = require('request');

var ENTER = function () { console.log(arguments.callee.name + ": entering."); };
var EXIT = function () { console.log(arguments.callee.name + ": exiting."); };

var PNG_ROOT_DIR = '/datartemix/ALMA/PNG';

var miningPyHost = "http://juliette.obspm.fr"
var miningPyPort = "4250"

var useSAMP = true;

var renderingCapabilities = null;

/***************************************************/
/* Define the interface with the HTTP FITS server. */
/***************************************************/

var clienthttp = {
  server : miningPyHost+":"+miningPyPort+"/artemix",
  setData : function(fileName, sessionID, callback){
    request(this.server+"/setData?fileName="+fileName+"&sessionID="+sessionID, { json: true }, function(error, response, body){
      callback(error, response, body);
    });
  },

  degToHMSDMS : function(fileName, sessionID, ra, dec, callback){
    request(this.server+"/degToHMSDMS?fileName="+fileName+"&sessionID="+sessionID+"&ra="+ra+"&dec="+dec, { json: true }, function(error, response, body){
      callback(error, response, body);
    });
  },

  rangeToHMS : function(fileName, sessionID, start, end, step, callback){
    request(this.server+"/rangeToHMS?fileName="+fileName+"&sessionID="+sessionID+"&start="+start+"&end="+end+"&step="+step, { json: true }, function(error, response, body){
      callback(error, response, body);
    });
  },

  rangeToDMS : function(fileName, sessionID, start, end, step, callback){
    request(this.server+"/rangeToDMS?fileName="+fileName+"&sessionID="+sessionID+"&start="+start+"&end="+end+"&step="+step, { json: true }, function(error, response, body){
      callback(error, response, body);
    });
  },

  getSlice : function(fileName, sessionID, sliceNumb, step, callback){
    request(this.server+"/getSlice?fileName="+fileName+"&sessionID="+sessionID+"&sliceNumb="+sliceNumb+"&step="+step, { json: true }, function(error, response, body){
      callback(error, response, body);
    });
  },

  getAverage : function(fileName, sessionID, step, startY, endY, startX, endX, startZ, endZ, callback){
    request(this.server+"/getAverage?fileName="+fileName+"&sessionID="+sessionID+"&step="+step+"&startY="+startY+"&endY="+endY+"&startX="+startX+"&endX="+endX+"&startZ="+startZ+"&endZ="+endZ,
      { json: true }, function(error, response, body){
        callback(error, response, body);
    });
  },

  getFreq : function(fileName, sessionID, x, y, startZ, endZ, callback){
    request(this.server+"/getFreq?fileName="+fileName+"&sessionID="+sessionID+"&x="+x+"&y="+y+"&startZ="+startZ+"&endZ="+endZ,
      { json: true }, function(error, response, body){
        callback(error, response, body);
    });
  },

  createFits : function(relFITSFilePath, iRA, iDEC ,callback){
    let input = {
        "relFITSFilePath" : relFITSFilePath,
        "iRA" : iRA,
        "iDEC" : iDEC
      };
    request.post(this.server+"/createFits", {json: true, body: input}, function(error, response, body) {
       callback(error, response, body);
    });
  },

  getAverageSpectrum : function(relFITSFilePath, sessionID, startY, endY, startX, endX,retFITS, callback){
    request(this.server+"/getAverageSpectrum?relFITSFilePath="+relFITSFilePath+"&sessionID="+sessionID+"&startY="+startY+"&endY="+endY+"&startX="+startX+"&endX="+endX+"&retFITS="+retFITS,
      { json: true }, function(error, response, body){
        callback(error, response, body);
    });
  },

  getSumOverSliceRectArea : function(relFITSFilePath, sessionID, sliceIndex, RAPix0, RAPix1, DECPix0, DECPix1, callback) {
    request(this.server+"/getSumOverSliceRectArea?relFITSFilePath="+relFITSFilePath+
      "&sessionID="+sessionID+
      "&sliceIndex="+sliceIndex+
      "&RAPix0="+RAPix0+
      "&RAPix1="+RAPix1+
      "&DECPix0="+DECPix0+
      "&DECPix1="+DECPix1,
      {json: true},
      function(error, response, body){
        callback(error, response, body)
      });
  },

  RADECRangeInDegrees : function(fileName, sessionID, callback){
    request(this.server+"/RADECRangeInDegrees?fileName="+fileName+"&sessionID="+sessionID, { json: true }, function(error, response, body){
      callback(error, response, body);
    });
  },

  getHeader : function (fileName, sessionID, callback) {
    request(this.server+"/getHeader?fileName="+fileName+"&sessionID="+sessionID, { json: true }, function(error, response, body){
      callback(error, response, body);
    });
  },

  getOneSliceAsPNG : function(relFITSFilePath, sliceIndex, ittName, lutName, vmName, sessionID, callback) {
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "sliceIndex": sliceIndex,
      "ittName" : ittName,
      "lutName" : lutName,
      "vmName" : vmName,
      "sessionID": sessionID
    };
    request.post(this.server+"/getOneSliceAsPNG", {json: true, body:input}, function(error, response, body){
      callback(error, response, body);
    });
  },

  getSummedSliceRangeAsPNG: function(relFITSFilePath, sliceIndex0, sliceIndex1, ittName, lutName, vmName, sessionID, callback ) {
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "sliceIndex0": sliceIndex0,
      "sliceIndex1": sliceIndex1,
      "ittName" : ittName,
      "lutName" : lutName,
      "vmName" : vmName,
      "sessionID": sessionID
    };
    request.post(this.server+"/getSummedSliceRangeAsPNG", {json: true, body:input}, function(error, response, body){
      callback(error, response, body);
    });
  },


  renderingCapabilities: function(callback) {
      request(this.server+"/renderingCapabilities", {json: true}, function (error, response, body){
	  callback(error, response, body);
      });
  },

  ping: function(callback) {
      request(this.server+"/ping", {json: true}, function (error, response, body){
	  callback(error, response, body);
      });

  }
}

/******************/
/*   GET routes   */
/******************/


var filterHttpResponse = function (res, error, response, body) {
    if (error) {
        var message = error.toString();
	      res.send(JSON.stringify({"status" : false, "message" : "Internal error : " + message + ". Maybe the server is simply not running", "result" : null}));
    }
    else if ( response["statusCode"] == 500) {
      res.send(JSON.stringify({"status" : false, "message": response["body"]}));
    }
    else if ( body["status"] == false) {
      res.send(JSON.stringify({"status" : false, "message": body["message"] }));
    }
    return;
};

/* GET home page. */
router.get('/',

/* Check that the FITS server is up and waiting*/
function(req, res, next) {
    console.log("pinq http request callback: arming");
    clienthttp.ping( (error, response, body) => {
      console.log("pinq http request callback: entering")
      filterHttpResponse(res, error, response, body);
      console.log ("FITS server is up and waiting " + JSON.stringify(body, 0, 4));
      console.log("pinq http request callback: exiting")
      next();
     });
    console.log("pinq http request callback: armed");
},

/* Retrieve the rendering capabilities */
function(req, res, next) {
    console.log("renderingCapabilities http request callback: arming");
    clienthttp.renderingCapabilities( (error, response, body) => {
      console.log("renderingCapabilities http request callback: entering")
      filterHttpResponse(res, error, response, body);
      console.log("Here are the rendering capabilities : " + JSON.stringify(body, 0, 4));
      console.log("renderingCapabilities http request callback: exiting")
      renderingCapabilities = body["result"];
      next();
    });
    console.log("renderingCapabilities http request callback: armed");
},

function(req, res) {
  ENTER();

  var path_to_fits = req.query.path;
  // retrieve the product name, i.e. the simple filename without extension nor prefix.
  var product = path.parse(req.query.path).name;
  console.log("product = " + product)

  clienthttp.getHeader(path_to_fits, req.query.sessionID ? req.query.sessionID : 0, (error, response, body) => {
    if (error) {
      var message = error.toString();
      res.send(JSON.stringify({ error: message }));
    }
    else if (body["status"] == false){
      let params = {
        title: 'Loading ' + path_to_fits,
        path_to_fits: path_to_fits,
        useSAMP:useSAMP
      }
      res.render('setData', params);
    }
    else {
      header = JSON.parse(body["result"]);
      useSAMP = header["INSTRUME"] != "SITELLE";
        
      // Let's render the page depending on the dimensionality of the dataset.

      // Ok it's a regular 3D dataset
      if ((header["NAXIS"] == 3 && header["NAXIS3"] > 1) || ( header["NAXIS"] == 4 && header["NAXIS3"] >1 && header["NAXIS4"]==1 )) {
        let params = {
          title: 'View of '+path_to_fits,
          path_to_fits: path_to_fits,
          product: product,
          header: header,
          useSAMP: useSAMP,
          renderingCapabilities: renderingCapabilities
        };
        res.render('olqv', params);
      }
      // Ok it's a 2D dataset ?
      else if ((header["NAXIS"] == 2 && header["NAXIS1"] > 1 && header["NAXIS2"] > 1) ||
        ( header["NAXIS"] == 4 && header["NAXIS1"] > 1 && header["NAXIS2"] > 1 && header["NAXIS3"] == 1 && header["NAXIS4"] == 1 )) {

        let params = {
          title: 'View of '+path_to_fits,
          path_to_fits: path_to_fits,
          product: product,
          header: header,
          useSAMP: useSAMP,
          renderingCapabilities: renderingCapabilities
        };
        console.log("About to render olqv_2d with params = " + JSON.stringify(params, 0,4 ));
        res.render('olqv_2d', params)
      }
      // Ok it's something that we don't know how to display ?
      else {
        let params = {
          title : 'No display',
          path_to_fits: path_to_fits,
          header : header
        }
        res.render('olqv_error', params);
      }
    }
  });
  EXIT();
});

router.get('/setData', function (req, res, next){
  ENTER();
  console.log("path_to_fits=", req.query.path);
  // retrieve the product name, i.e. the simple filename without extension nor prefix.
  clienthttp
  let params={title:'Loading data in memory', path: req.query.path};
  res.render('setData', params);
  EXIT();
});

/*
router.get('/sumpng', function (req, res, next) {
  var sliceIndex0 = req.query.si0;
  var sliceIndex1 = req.query.si1;
  var path_to_fits = req.query.path;
  var sessionID = req.query.sessionID;

  client.invoke("getSummedSliceRangeAsPNG", req.sessionID, sliceIndex0, sliceIndex1, path_to_fits, function (error, png, more) {
    if (error){
      console.log(error);
    }
    else {
      res.set({'Content-type': 'image/png'});
      res.send(png);
    }
  });
});
*/

/* A route to retrieve a PNG file indexed by a slice index */
router.get('/:product/:sliceIndex', function(req, res, next) {
  sliceIndex = req.params['sliceIndex'];
  product = req.params['product'];

  console.log(JSON.stringify(req.params, 0, 4));
  var files = fs.readdirSync(PNG_ROOT_DIR+product);
  console.log("Number of files " + files.length);
  let sliceNumber = files.length;

  fs.createReadStream(PNG_ROOT_DIR+product+'/'+sliceIndex+'.png')
  .pipe(new PNG({
    filterType: -1
  }))
  .on('metadata', function() {
    res.render('index', { title: 'Express', op: 'slice', sliceIndex:sliceIndex, product:product, width:this.width, height:this.height, sliceNumber : sliceNumber });
  });

});

/* A route to display a spectrum */
router.get('/spectrum/:path'), function(req, res, next) {
  path = req.params['path'];

  client.invoke("setData", path, req.sessionID ?req.sessionID : 0, function (error, res, more) {
    if (error) {
      var message = error.toString();
      console.log(message);
    }
    else {
      header = JSON.parse(res);
      res.render('index', {title: 'Express', op: 'spectrum', header:header})
    }
  });
}

/******************/
/*   POST routes  */
/******************/

/* The index post route. It will trigger a behaviour based on the value of "method" parameter */
router.post('/', function(req, res, next) {
  console.log("router.post('/', function(req, res, next) { : entering");
  console.log('method ='+req.body.method);

  var method = req.body.method;
  var self = res;
  var header;


  if (method === "getHMSDMS") {
    var ra = req.body.ra;
    var dec = req.body.dec;

    clienthttp.degToHMSDMS(req.body.fileName, req.sessionID ?req.sessionID : 0, parseInt(ra), parseInt(dec), (error, response, body)=>{
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: body }));
    });
  }

  if (method === "getRangeHMS") {
    var startR = req.body.start;
    var endR = req.body.end;
    var step = req.body.step;

    clienthttp.rangeToHMS( req.body.fileName, req.sessionID ?req.sessionID : 0, startR, endR, step, (error, response, body)=>{
      console.log("rangeToHMS");
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: body }));
    });
  }

  else if (method === "getRangeDMS") {
    let startR = req.body.start;
    let endR = req.body.end;
    let step = req.body.step;

    clienthttp.rangeToDMS(req.body.fileName, req.sessionID ?req.sessionID : 0, startR, endR, step, (error, response, body)=>{
      console.log("rangeToDMS");
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: body }));
    } );
  }

  else if (method == "RADECRangeInDegrees") {
      clienthttp.RADECRangeInDegrees(req.body.fileName, req.body.sessionID ?req.body.sessionID : 0, (error, response, body) => {
        self.setHeader('Content-type', 'application/json');
        console.log("response = " + response + ", body = " + body);
        let result = {data: body};
        console.log ("About to return '" + JSON.stringify(result, 0, 4) + "'");
        self.send(JSON.stringify(result));
      });
  }

  else if (method === "getHeader") {
    var fileName = req.body.name;

    clienthttp.setData(fileName, req.sessionID ?req.sessionID : 0, (error, response, body)=>{
      if (error) {
        var message = error.toString();
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ error: message }));
      } else {
        header = JSON.parse(body);
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: {displayLimits: displayLimits, header:header }}));
      }
    });
  }

  else if(method === "getSlice"){
    var sliceB = req.body.slice ? req.body.slice : 0;
    step = parseInt(req.body.step) ? parseInt(req.body.step) : 1;
    clienthttp.getSlice(req.body.fileName, req.sessionID ?req.sessionID : 0, sliceB, step, (error, response, body)=>{
      console.log("getSlice: ");
      self.setHeader('Content-Type', 'application/json');
      console.log("getSlice : start sending slice to client");
      self.send(JSON.stringify({ data: body }));
      console.log("getSlice : finished sending slice to client");
    });
  }

  else if (method === "getAverage") {
    var zmin = parseInt(req.body.zmin) ? parseInt(req.body.zmin) : 0;
    var zmax = parseInt(req.body.zmax) ? parseInt(req.body.zmax) : null;
    step = parseInt(req.body.step) ? parseInt(req.body.step) : 1;
    console.log("### average values :  "+ zmin + "   "+zmax+"   "+step);
    clienthttp.getAverage(req.body.fileName, req.sessionID ?req.sessionID : 0, step, null, null, null, null, zmin, zmax, (error, response, body)=>{
      self.setHeader('Content-Type', 'application/json');
      console.log("getAverage : start sending slice to client");
      self.send(JSON.stringify({ data: body }));
      console.log("getAverage : finished sending slice to client");
    });
  }

  else if(method === "getFreq") {
    var x = req.body.x ? req.body.x : 0;
    var y = req.body.y ? req.body.y : 0;

    clienthttp.getFreq(req.body.fileName, req.sessionID ? req.sessionID:0, parseInt(x), parseInt(y), null, null, (error, response, body)=>{
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: body }));
    });
  }

  else if(method === "getAverageSpectrum") {
    console.log("About to execute the code for getAverageSpectrum");
    var xmin = req.body.xmin ? parseInt(req.body.xmin) : 0;
    var ymin = req.body.ymin ? parseInt(req.body.ymin) : 0;
    var xmax = parseInt(req.body.xmax)|| null;
    var ymax = parseInt(req.body.ymax) || null;

    var retFITS = useSAMP;
    clienthttp.getAverageSpectrum(req.body.relFITSFilePath, req.sessionID ?req.sessionID : 0, ymin, ymax, xmin, xmax, retFITS, (error, response, body)=>{
      console.log("error = " + JSON.stringify(error));
      //console.log("response = " + JSON.stringify(response, 0, 4));
      //console.log("body = " + JSON.stringify(body, 0, 4));
      if (error) {
        var message = error.toString();
        console.log(message);
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({"status" : false, "message": message }));
      }
      else if (body["status"] == false) {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({"status" : false, "message": body["message"] }));
      }
      else {
        if (useSAMP) {
          //create temporary file in public/SAMP/
          var tempFile = "averageSpectrum."+uuidv1()+".fits";
          var path = "public/"+tempFile;
          //fs.writeFileSync(path,res,{encoding:'utf8',flag:'w'})
          fitsContent = body["result"]["averageSpectrumFits"];
          fs.writeFile(path, fitsContent, function(error, fitsContent){
            if (error) {
            var message = error.toString();
            console.log(message);
            res.send(JSON.stringify({"status" : false, "message": message}));
          }
            else {
              console.log("sending the name of the tmp FITS file along with the averageSpectrum");
              res.send(JSON.stringify({"status": true,
                                        "message": "",
                                        "result": { "absFITSFilePath": "/"+tempFile,
                                                    "averageSpectrum": body["result"]["averageSpectrum"]
                                                  }
                                      }
                                    )
                      );
            }
          });
        }
        else {
          console.log("sending only the averageSpectrum")
          res.send(JSON.stringify({"status": true, "message": "", "result": {"absFITSFilePath": "", "averageSpectrum": body["result"]["averageSpectrum"]}}));
        }
      }
    });
  }

  else if (method === "getSumOverSliceRectArea") {
    console.log("About to execute the code for getSumOverSliceRectArea");
    clienthttp.getSumOverSliceRectArea(
      req.body.relFITSFilePath,
      req.body.sessionID ? req.body.sessionID : 0,
      req.body.sliceIndex ? parseInt(req.body.sliceIndex) : 0,
      req.body.RAPix0 ? parseInt(req.body.RAPix0) : 0,
      req.body.RAPix1 ? parseInt(req.body.RAPix1) : null,
      req.body.DECPix0 ? parseInt(req.body.DECPix0) : 0,
      req.body.DECPix1 ? parseInt(req.body.DECPix1) : null,
      (error, response, body) => {
        console.log(JSON.stringify(body, 0, 4));
        console.log(JSON.stringify(response, 0, 4));
        if (error) {
          var message = error.toString();
          console.log(message);
          res.send(JSON.stringify({"status" : false, "message": message }));
        }
        else if ( response["statusCode"] == 500) {
          self.send(JSON.stringify({"status" : false, "message": response["body"]}));
        }
        else if ( body["status"] == false) {
          self.send(JSON.stringify({"status" : false, "message": body["message"] }));
        }
        else {
          self.send(JSON.stringify({"status" : true, "message" : "", "result" : body["result"] }))
        }
      }
    );
  }
  else if(method === "createFits") {
    var iRA  = req.body.iRA;
    var iDEC = req.body.iDEC;
    var relFITSFilePath = req.body.relFITSFilePath
    clienthttp.createFits(relFITSFilePath, iRA, iDEC ,(error, response, body)=>{
      //console.log("error = " + JSON.stringify(error));
      //console.log("response = " + JSON.stringify(response));
      //console.log("body = " + JSON.stringify(body));
      if (error) {
        var message = error.toString();
        console.log(message);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({"status" : false, "message": message }));
      }
      else if (body["status"] == false) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({"status" : false, "message": body["message"] }));
      }
      else {
        console.log("createFits spectrum: with parameters ("+iRA+","+iDEC+")");
        //create temporary file in public/SAMP/
        var tempFile = "spectrum."+uuidv1()+".fits";
        var path = "public/"+tempFile;
        //fs.writeFileSync(path,res,{encoding:'utf8',flag:'w'})
        fitsContent = body["result"];
        fs.writeFile(path, fitsContent, function(error, fitsContent){
          if (error) {
            var message = error.toString();
            console.log(message);
            res.send(JSON.stringify({"status" : false, "message": message}));
          }
          else {
            console.log("Successfully Written to File.");
            res.send(JSON.stringify({"status": true, "message": "", "result": "/"+tempFile}));
          }
        });
      }
    });
  }

  else if (method === "getObjects") {
    // Peform a distinct query against the a field Header.OBJECT . But don't forget to reject the null Header.OBJECT !
    Fitsinfo.distinct('Header.OBJECT', {'Header.OBJECT' : {$ne: null}},function(err, objects) {
      if (err) {
        console.log(err);
      } else {
          self.setHeader('Content-Type', 'application/json');
          self.send(JSON.stringify({ data: objects.sort(function (a, b) {
            var aA = a.toUpperCase();
            var bB = b.toUpperCase();
            return (aA < bB) ? -1 : (aA > bB) ? 1 : 0;
          })
        }));
      }
    });
  }

  else if (method === "getFiles") {
    var object = req.body.object;

    // Peform a distinct query against the a field Path with field Header.OBJECT == object
    Fitsinfo.distinct('Path', {"Header.OBJECT": object}, function (err, files) {
      if (err) {
        console.log(err);
      } else {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({data: files}));
      }
    });
  }

  else if (method === "getFitsHeader") {
    var path = req.body.path;

    // Find the corresponding entry in DB
    //{fields: {"_id": 0}
    Fitsinfo.find({"Path": path}, {"_id": 0}, function(err, fitsHeader) {
      if (err) {
        console.log(err);
      } else {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({data: fitsHeader[0]}));
      }
    });
  }


  else if (method === "getHistory") {
    self.setHeader('Content-Type', 'application/json');
    self.send(JSON.stringify({data: req.user.recentFiles}));
  }
  else {
    console.log("Do not know what to do here with " + JSON.stringify(req.body, null, 4));
  }

  console.log("router.post('/', function(req, res, next) { : exiting");

});

/*
** In response to a POST, returns a slice of the current datacube as a 2D image in PNG format
** along with statistics on the physical values of the slice.
** the slice index, i.e. the index corresponding to one given frequency, is
** passed in the field 'si' of the query along with some statistics on the
**
*/
router.post('/setData', function(req, res, next) {
    console.log("router.post('/setData', function(req, res, next) { : entering");
    // we need a FITS header
    clienthttp.setData(encodeURI(req.body.fileName), req.sessionID ?req.sessionID : 0, (error, response, body) => {
      if (error) {
        var message = error.toString();
        console.log(message);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({"status" : false, "message": message }));
      }
      else if (body["status"] == false) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({"status" : false, "message": body["message"] }));
      }
      else {
        console.log(JSON.stringify(body, 0, 4));
        //var header = JSON.parse(res_header);
        var header = JSON.parse(body["result"]);
        var product = path.parse(req.body.fileName).name;
        console.log("product=", product);
        let params = {
          title: 'Express',
          path: encodeURI(req.body.fileName),
          product: product,
          header: header
        };
        res.redirect('/?path='+encodeURI(req.body.fileName));
      }
    });
    console.log("router.post('/setData', function(req, res, next) { : exiting");
});

router.post('/png', function(req, res, next) {
  console.log("router.post('/png', function(req, res, next) { : entering");

  clienthttp.getOneSliceAsPNG(req.body.path, req.body.si, req.body.ittName, req.body.lutName, req.body.vmName, req.body.sessionID ? req.body.sessionID : 0, (error, response, body)=>{
    console.log('clienthttp.getOneSliceAsPNG(req.body.path, req.body.si, req.body.sessionID, (error, response, body)=>{ : entering');
    if (error) {
      console.log(error);
    }
    else {
      res.send(body);
    }
    console.log('clienthttp.getOneSliceAsPNG(req.body.path, req.body.si, req.body.sessionID, (error, response, body)=>{ : exiting');
  });

  console.log("router.post('/png', function(req, res, next) { : exiting");
});

router.post('/sumpng', function(req, res, next) {
  console.log("router.post('/sumpng', function(req, res, next) {: entering");

  clienthttp.getSummedSliceRangeAsPNG(req.body.path, req.body.si0, req.body.si1, req.body.ittName, req.body.lutName, req.body.vmName, req.body.sessionID ? req.body.sessionID : 0, (error, response, body) => {
    console.log("clienthttp.getSummedSliceRangeAsPNG, req.body.path, req.body.si0, req.body.si1, req.body.sessionID, (error, response, body) => { entering");
    if (error){
      console.log(error);
    }
    else {
      //result["path_to_png"] = result["path_to_png"].toString(); // ??? why do I get an object instead of a string . I have no idea; this does not happen with post('/png') !!!
      res.send(body);
    }
    console.log("clienthttp.getSummedSliceRangeAsPNG, req.body.path, req.body.si0, req.body.si1, req.body.sessionID, (error, response, body) => { exiting");
  });
  console.log("router.post('/sumpng', function(req, res, next) {: exiting");
})

module.exports = router;
