/*
** A set of classes and function definitions utilized by the
** differents flavours of OLQV viewers.
**
** Author : M. Caillat
** Date : 06th December 2018
*/

/*
** A class to convert a right ascension expressed in decimal degree into an integer value expressing a pixel index.
*/
let RADDtoPixelConverter = function(radd0, radd1, rapix0, rapix1) {
    console.log("let RADDtoPixelConverter = function(radd0, radd1, rapix0, rapix1) { : entering");
    let _radd0 = radd0;
    let _rapix0 = rapix0;
    let _slope = (rapix1 - rapix0) / (radd1 - radd0);
    console.log("_radd0 = " + _radd0 + ", _rapix0 = " + _rapix0);

    this.convert = function(radd) {
      return _rapix0 + (radd - _radd0) * _slope;
    }
    console.log("let RADDtoPixelConverter = function(radd0, radd1, rapix0, rapix1) { : exiting");
};

/*
** A class to convert a declination expressed in decimal degree into an integer value expressing a pixel index.
*/
let DECDDtoPixelConverter = function(decdd0, decdd1, decpix0, decpix1) {
    let _decdd0 = decdd0;
    let _decpix0 = decpix0;
    let _slope = (decpix1 - decpix0) / (decdd1 - decdd0);

    this.convert = function(decdd) {
      return _decpix0 + (decdd - _decdd0) * _slope;
    }
};

/*
** Converts a decimal number expected to represent an angle in degree
** into a string expressing a right ascension ( H:M:S)
*/
let DecDeg2HMS = function(deg,sep=':'){
  //if(any(deg< 0 | deg>360)){stop('All deg values should be 0<=d<=360')}
  //if (deg < 0)
  //deg[deg < 0] = deg[deg < 0] + 360
  HRS = Math.floor(deg/15);
  MIN = Math.floor((deg/15 - HRS) * 60);
  SEC = (deg/15 - HRS - MIN/60) * 3600;
  SEC = Math.floor(SEC*1000) / 1000.;
  return HRS+sep+MIN+sep+SEC.toFixed(3);
};

/*
** Converts a decimal number expected to represent an angle in degree
** into a string expressing a declination ( D:M:S)
*/
let DecDeg2DMS = function(deg,sep=':'){
  let sign = deg < 0 ? '-':'+';
  deg = Math.abs(deg);
  let DEG = Math.floor(deg);
  let MIN = Math.floor((deg - DEG) * 60);
  let SEC = (deg - DEG - MIN/60) * 3600;
  SEC = Math.floor(SEC*1000.) / 1000.;
  if (SEC < 0.) SEC = 0.; if (SEC > 60) SEC = 60.;

  return(sign + DEG + ':' + MIN + ':' + SEC.toFixed(3));
};

/*
** A class to convert pixels into a string expressing a right ascension.
**
** The constructor establishes the transformation pixels -> HMS
** with the given parameter ra0pix, ra1pix ( interval in pixels )
** and ra0, ra1 ( the same interval in decimal degrees)
*/
function  raLabelFormatter (ra0pix, ra1pix, ra0, ra1) {
  let _ra0pix = ra0pix;
  let _ra1pix = ra1pix;
  let _ra0 = ra0;
  let _ra1 = ra1;
  let _slope = ((_ra1 - _ra0) / (_ra1pix - _ra0pix))

  /*
  ** Returns the string representation of a RA in HMS given its input value in pixels.
  */
  this.format = function(rapix) {
    let res = _ra0 + (rapix - _ra0pix) * _slope;
    return DecDeg2HMS(res);
  }
};

/*
** A class to convert pixels into a string expressing a declination.
**
** The constructor establishes the transformation pixels -> DMS
** with the given parameter dec0pix, dec1pix ( interval in pixels )
** and dec0, dec1 ( the same interval in decimal degrees)
*/
function decLabelFormatter (dec0pix, dec1pix, dec0, dec1) {
  let _dec0pix = dec0pix;
  let _dec1pix = dec1pix;
  let _dec0 = dec0;
  let _dec1 = dec1;
  let _slope = ((_dec1 - _dec0) / (_dec1pix - _dec0pix))

  this.format = function(decpix) {
    let res = _dec0 + (decpix - _dec0pix) * _slope;
    return DecDeg2DMS(res);
  }
};

/*
** A function which returns the units of a spectrum resulting
** from the summation of the pixels values on each plane of a range of RA-DEC planes in a FITS cube.
*/
function summedPixelsSpectrumUnit(unit) {
  switch (unit) {
    case "Jy/beam":
      return "Jy/beam * km/s";
      break;

    case "erg/s/cm^2/A/arcsec^2":
      return "erg/s/cm^2/A";
      break;

    default:
      return "";
  }
}


/*
** A function which returns the units of a 2D array resulting from
** the summation of a range of RA-DEC planes in a FITS cube.
*/
function summedPixelsUnit(unit) {
  switch (unit) {
    case "Jy/beam":
      return "Jy/beam * km/s";
      break;

    case "erg/s/cm^2/A/arcsec^2":
      return "erg/s/cm^2/arcsec^2";
      break;

    default:
      return "";
  }
}

/*
** A function which returns a factor
** for the display of physical quantity
** The returned values are based on experience rather
** than on a purely rational approach
*/
function unitRescale(unit) {
  switch (unit) {
    case "Jy/beam":
      return 1.0;
      break;

    case "erg/s/cm^2/A/arcsec^2":
//     return 1e18;
      return 1.;
      break;

    case "erg/s/cm^2/A":
//      return 1e12;
      return 1.;

    default:
      return 1.0;

  }
}

/*
** A function which sums the values of a array
** between two indices i0 (included ) and i1 ( excluded )
** and returns the sum multiplied by a coefficient coeff.
*/
function sumArr(arr, i0, i1, coeff) {
    i0 = Math.max(0, i0);
    i1 = Math.min(arr.length-1, i1);

    if (i0 > i1) [i0, i1] = [i1, i0];
    return coeff * arr.slice(i0, i1).reduceRight(function(a, b) {return a + b;});
}
