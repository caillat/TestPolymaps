#!/usr/bin/env python
#
# --------------------------------------------------------------------------------------------
# This script is made to be run by cron at regular interval of time.
# It checks if a given application is running
# It this is not the case it launches it.
#
# Michel Caillat
# January 8th, 2018
# 3 septembre 2018
# 25 novembre 2018
#---------------------------------------------------------------------------------------------
import psutil, sys, subprocess, os, shlex
import smtplib
import socket

from email.mime.text import MIMEText

#
# Where are we ?
#
OLQVDir = os.path.dirname(os.path.realpath(__file__))

#
# What's the command we are looking for ?
#
lookingFor = OLQVDir + "/bin/www"
print ("looking for '%s' in the result of a ps aux." % lookingFor)

#
# Are we running the server already ?
#
#        for item in p.cmdline():

for p in psutil.process_iter():
    try:
        for item in p.cmdline():
            item = p.cmdline()[-1]
            if str(lookingFor) == str(item) :
                pinfo = p.as_dict(attrs=['pid'])
                print ("----------------------------")
                print (item + " " + lookingFor)
                print ("ARTEMIX '%s' server is running in process #%d" % (lookingFor, pinfo['pid']))
                sys.exit(1)
    except psutil.NoSuchProcess:
        pass

#
# Who are we ?
#
hostname = socket.gethostname()

#
# Which command do we want to launch
#

#
# Define the PORT to be listened at
#
port = "3001"
d = dict(os.environ.copy())
d["PORT"]=port

#
# Define the command to be launched
#
command = "/usr/bin/nohup %s"% (lookingFor)
args = shlex.split(command)

#
# The logging output
#
nohup_out = open(OLQVDir+"/nohup.out", "a")

#
# And go !
#
try :
    print ("About to start command %r in a subprocess" % args)
    subprocess.Popen(args, stdout=nohup_out, stderr=nohup_out, cwd=OLQVDir, env=d)
except Exception as e :
    print (e.child_traceback)

#
# Send an email about the restarting of the server.
#
COMMASPACE = ', '
artemixCrew = ['Philippe.Salome@obspm.fr', 'yaye-awa.ba@obspm.fr', 'Michel.Caillat@obspm.fr']

msg = MIMEText('Everything\'s in the subject')
msg['Subject'] =  "'" + command + "' has been relaunched on " + hostname +" ."
msg['From'] = 'partemix@obspm.fr'
msg['To'] = COMMASPACE.join(artemixCrew)
server = smtplib.SMTP('smtp.obspm.fr')
server.sendmail('artemix.lerma@obspm.fr', 'michel.caillat@obspm.fr', msg.as_string())
server.quit()
